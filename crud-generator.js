const fs = require('fs');
const { serivce } = require('./crud-generator/file-service-default');
const { handler } = require("./crud-generator/file-handler-default");
const { serializer } = require('./crud-generator/file-serializer-default');

//############## ALTERAR AQUI PARA CRIAR UMA NOVA CLASSE;

//NOME DA TABELA QUE SERÁ UTILIZADA NO BANCO 
const newClass = "Bugs";
const newClassPath = "bugs";

//NOME DAS CLASSES DO PRISMA
const newClassNamePrismaModel = "Bug";
const newClassNamePrismaDatabase = "bug";
//############## FIM;

const contentService = serivce(newClass, newClassNamePrismaModel, newClassNamePrismaDatabase);
const nameFileService = `${newClassPath}.service.ts`;
fs.writeFile(`src/services/core/${nameFileService}`, contentService, err => {
  if (err) {
    console.error(err);
  }
});

const contentHandler = handler(newClass, newClassPath);
const nameFileHandler = `${newClassPath}.handler.ts`;

fs.writeFile(`src/handlers/core/${nameFileHandler}`, contentHandler, err => {
    if (err) {
      console.error(err);
    }
});

const contentSerializer = serializer();
const nameFileSerializer = `${newClassPath}.serializer.ts`;

fs.writeFile(`src/serializers/core/${nameFileSerializer}`, contentSerializer, err => {
    if (err) {
      console.error(err);
    }
});

const contentIndex = `\nimport { ${newClass}Handler } from "./handlers/core/${newClassPath}.handler";\nnew ${newClass}Handler();`

fs.appendFile('src/index.ts', contentIndex, function (err) {
  if (err) {
      console.error(err);
  } 
})