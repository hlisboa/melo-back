const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const user_roles = require("./modules/user_roles");
const internal_configs = require("./modules/internal_config");
const users = require("./modules/users");
const project_status = require("./modules/project_status");
const project_picture = require("./modules/project_pictures");
const test_case_type = require("./modules/test_case_type");
const organizations = require("./modules/organizations");
const projects = require("./modules/projects");
const execution_status = require("./modules/execution_status");
const test_case_status = require("./modules/test_case_status");
const tests_cycle_status = require("./modules/tests_cycle_status");

async function main() {
    await user_roles.insertRoles(prisma);
    await internal_configs.insertConfigs(prisma);
    await users.insertUsers(prisma);
    await project_status.insertStatus(prisma);
    await project_picture.insertPictures(prisma);
    await test_case_type.insertTestCaseTypes(prisma);
    await test_case_status.insertTestCaseStatus(prisma);
    await tests_cycle_status.insertTestsCycleStatus(prisma);

    // Default creation
    await users.createDefaultUsers(prisma);
    await organizations.createDefaultOrganizations(prisma);
    await projects.createDefaultProjects(prisma);
    await execution_status.createExecutionStatus(prisma);
}

main()
.then(async () => {
    await prisma.$disconnect();
})
.catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
});