module.exports.insertStatus = async (prisma) => {
    const all_status = [
        {
            id: 1,
            name: "Ativo",
            value: "Ativo"
        },
        {
            id: 2,
            name: "Encerrado",
            value: "Encerrado"
        }
    ];

    console.log("Creating Project Status...");
    for(const status of all_status) {
        const result = await prisma.projectStatus.upsert({
            where: {
                id: status.id
            },
            update: {
                name: status.name,
                value: status.value
            },
            create: {
                id: status.id,
                name: status.name,
                value: status.value
            }
        });
        console.log(result);
    }
    console.log("End creation of Project Status.\n");

};