module.exports.insertTestsCycleStatus = async (prisma) => {
    const tests_cycle_status = [
        {
            id: 1,
            name: "Aguardando",
        },
        {
            id: 2,
            name: "Em progresso",
        },
        {
            id: 3,
            name: "Concluído",
        }
    ];

    console.log("Creating Tests Cycle Status...");
    for (const t_status of tests_cycle_status) {
        const result = await prisma.testsCycleStatus.upsert({
            where: {
                id: t_status.id
            },
            update: {
                name: t_status.name,
            },
            create: {
                id: t_status.id,
                name: t_status.name,
            }
        });
        console.log(result);
    }
    console.log("End creation of Tests Cycle Status.\n");

};