module.exports.insertUsers = async (prisma) => {

    const all_users = [
        // {
        //     email: "disabled@disabled.com",
        //     name: "Testador Desabilitado",
        //     provider: "meloqa",
        //     confirmed: false
        // }
    ];

    console.log("\nCreating Users...");
    for(const user of all_users) {
        const result = await prisma.user.upsert({
            where: {
                email: user.email
            },
            update: {},
            create: {
                email: user.email,
                name: user.name,
                provider: user.provider,
                confirmed: user.confirmed
            }
        });
        console.log(result);
    }
    console.log("End creation of Users.");

};

module.exports.createDefaultUsers = async (prisma) => {

    const all_users = [
        {
            id: 1,
            email: "defaultUser@default.com",
            name: "Default User",
            provider: null,
            confirmed: false,
            isDisabled: true
        }
    ];

    console.log("\nCreating Default Users...");
    for(const user of all_users) {
        const result = await prisma.user.upsert({
            where: {
                email: user.email
            },
            update: {
                email: user.email,
                name: user.name,
                provider: user.provider,
                confirmed: user.confirmed,
                isDisabled: user.is_disabled
            },
            create: {
                id: user.id,
                email: user.email,
                name: user.name,
                provider: user.provider,
                confirmed: user.confirmed,
                isDisabled: user.is_disabled
            }
        });
        console.log(result);
    }
    console.log("End creation of Default Users.");
};