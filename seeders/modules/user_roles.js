module.exports.insertRoles = async (prisma) => {
    const all_roles = [
        {
            id: 1,
            role: "org_adm",
            name: "Administrador de organização"
        },
        {
            id: 2,
            role: "project_adm",
            name: "Administrador de projetos"
        },
        {
            id: 3,
            role: "tester",
            name: "Testador"
        },
        {
            id: 4,
            role: "ready_only",
            name: "Visualizador"
        },
        {
            id: 5,
            role: "meloqa_adm",
            name: "MeloQA"
        }
    ];

    console.log("Creating User Roles...");
    for(const role of all_roles) {
        const result = await prisma.userRole.upsert({
            where: {
                id: role.id
            },
            update: {
                role: role.role,
                name: role.name
            },
            create: {
                id: role.id,
                role: role.role,
                name: role.name
            }
        });
        console.log(result);
    }
    console.log("End creation of User Roles.\n");

};