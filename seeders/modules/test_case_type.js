module.exports.insertTestCaseTypes = async (prisma) => {
    const test_case_types = [
        {
            id: 1,
            name: "Resultado esperado por passo",
            type: "expected_result_per_step"
        },
        {
            id: 2,
            name: "Resultado esperado geral",
            type: "general_expected_result"
        },
        {
            id: 3,
            name: "BDD",
            type: "bdd"
        }
    ];

    console.log("Creating Test Case Types...");
    for(const t_type of test_case_types) {
        const result = await prisma.testCaseType.upsert({
            where: {
                id: t_type.id
            },
            update: {
                name: t_type.name,
                type: t_type.type
            },
            create: {
                id: t_type.id,
                name: t_type.name,
                type: t_type.type
            }
        });
        console.log(result);
    }
    console.log("End creation of Test Case Types.\n");

};