module.exports.createDefaultOrganizations = async (prisma) => {
    const all_organizations = [
        {
            id: 1,
            name: "MeloQA",
            owner_id: 1
        }
    ];

    console.log("\nCreating Default Organizations...");
    for(const organization of all_organizations) {
        const result = await prisma.organization.upsert({
            where: {
                id: organization.id
            },
            update: {
                name: organization.name,
                owner_id: organization.owner_id
            },
            create: {
                id: organization.id,
                name: organization.name,
                owner_id: organization.owner_id
            }
        });
        console.log(result);
    }
    console.log("End creation of Default Organizations.");
};