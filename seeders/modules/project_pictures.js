module.exports.insertPictures = async (prisma) => {

    // Urls novas
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Foguete.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Prosperidade.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Cerebro.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Metas.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Boia.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Calendario.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Engrenagem.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Acordo.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Caf%C3%A9.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Arquivo.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Mala.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Monitor.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Time.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Aviao-papel.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Quebra-cabecas.svg
    // https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Colalboracao.svg

    const all_pictures = [
        {
            id: 1,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Foguete.svg"
        },
        {
            id: 2,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Prosperidade.svg"
        },
        {
            id: 3,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Cerebro.svg"
        },
        {
            id: 4,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Metas.svg"
        },
        {
            id: 5,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Boia.svg"
        },
        {
            id: 6,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Calendario.svg"
        },
        {
            id: 7,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Engrenagem.svg"
        },
        {
            id: 8,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Acordo.svg"
        },
        {
            id: 9,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Caf%C3%A9.svg"
        },
        {
            id: 10,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Arquivo.svg"
        },
        {
            id: 11,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Mala.svg"
        },
        {
            id: 12,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Monitor.svg"
        },
        {
            id: 13,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Time.svg"
        },
        {
            id: 14,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Aviao-papel.svg"
        },
        {
            id: 15,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Quebra-cabecas.svg"
        },
        {
            id: 16,
            url: "https://meloqa-avatars.s3.amazonaws.com/project_pictures/Mello-projecticon_Colalboracao.svg"
        }
    ];

    console.log("Creating Project Pictures...");
    for(const picture of all_pictures) {
        const result = await prisma.projectPicture.upsert({
            where: {
                id: picture.id
            },
            update: {
                url: picture.url
            },
            create: {
                id: picture.id,
                url: picture.url
            }
        });
        console.log(result);
    }
    console.log("End creation of Project Pictures.\n");

};