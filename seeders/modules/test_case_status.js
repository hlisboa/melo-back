module.exports.insertTestCaseStatus = async (prisma) => {
    const test_case_status = [
        {
            id: 1,
            name: "Ativo"
        },
        {
            id: 2,
            name: "Encerrado",
        },
        {
            id: 3,
            name: "Rascunho",
        },
        {
            id: 4,
            name: "Revisão",
        }
    ];

    console.log("Creating Test Case Status...");
    for(const t_status of test_case_status) {
        const result = await prisma.testCaseStatus.upsert({
            where: {
                id: t_status.id
            },
            update: {
                name: t_status.name,
            },
            create: {
                id: t_status.id,
                name: t_status.name,
            }
        });
        console.log(result);
    }
    console.log("End creation of Test Case Status.\n");

};