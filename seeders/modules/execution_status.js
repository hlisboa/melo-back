module.exports.createExecutionStatus = async (prisma) => {
    const all_default_status = [
        {
            id: 1,
            name: "Aguardando",
            description: "",
            color: "#5AA7E5",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 2,
            name: "Em progresso",
            description: "",
            color: "#ffdc4d",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 3,
            name: "Falhou",
            description: "",
            color: "#FFC6D2",
            projectId: null,
            is_default: true,
            is_deleted: false,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 4,
            name: "Passou",
            description: "",
            color: "#B0E8B2",
            projectId: null,
            is_default: true,
            is_deleted: false,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 5,
            name: "Status 5",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 6,
            name: "Status 6",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 7,
            name: "Status 7",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 8,
            name: "Status 8",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 9,
            name: "Status 9",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        },
        {
            id: 10,
            name: "Status 10",
            description: "",
            color: "#00BCD4",
            projectId: null,
            is_default: true,
            is_deleted: true,
            deleted_by: null,
            deleted_at: null,
        }
    ];

    console.log("Creating Execution Status...");
    for(const status of all_default_status) {
        const result = await prisma.executionStatus.upsert({
            where: {
                id: status.id
            },
            update: {
                name: status.name,
                description: status.description,
                color: status.color,
                
            },
            create: {
                name: status.name,
                description: status.description,
                color: status.color,
                
            }
        });
        console.log(result);
    }
    console.log("End creation of Custom Default Execution Status..\n");
};