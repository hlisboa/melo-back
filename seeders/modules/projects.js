module.exports.createDefaultProjects = async (prisma) => {
    const all_projects = [
        {
            id: 1,
            name: "Projeto 1",
            status_id: 1,
            organizationId: 1,
            project_picture_url: 1,
            project_background_color: "#000000",
            userId: 1
        }
    ];

    console.log("\nCreating Default Projects...");
    for(const project of all_projects) {
        const result = await prisma.project.upsert({
            where: {
                id: project.id
            },
            update: {
                name: project.name,
                project_status: {
                    connect: {
                        id: project.status_id
                    }
                },
                organization: {
                    connect: {
                        id: project.organizationId,
                    }
                },
                project_url: {
                    connect: {
                        id: project.project_picture_url
                    }
                },
                project_owner: {
                    connect: {
                        id: project.userId
                    }
                },
                project_updater: {
                    connect: {
                        id: project.userId
                    }
                },
                project_background_color: project.project_background_color
            },
            create: {
                // id: project.id,
                name: project.name,
                project_status: {
                    connect: {
                        id: project.status_id
                    }
                },
                organization: {
                    connect: {
                        id: project.organizationId,
                    }
                },
                project_url: {
                    connect: {
                        id: project.project_picture_url
                    }
                },
                project_owner: {
                    connect: {
                        id: project.userId
                    }
                },
                project_updater: {
                    connect: {
                        id: project.userId
                    }
                },
                project_background_color: project.project_background_color
            }
        });
        console.log(result);
    }
    console.log("End creation of Default Projects.");
};