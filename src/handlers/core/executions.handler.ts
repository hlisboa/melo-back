import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ExecutionsService } from "../../services/core/executions.service";
import { multiple, single } from "../../serializers/core/executions.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ExecutionsHandler extends CrudHandler {
  
  @API("/executions/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ExecutionsService(payload).get(payload.params);;
  }

  @API("/executions", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ExecutionsService(payload).list(payload.params);
  }

  @API("/executions", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ExecutionsService(payload).create(payload.body);
  }

  @API("/executions-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ExecutionsService(payload).createList(payload.body);
  }

  @API("/executions/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ExecutionsService(payload).update(payload.body);
  }

  @API("/executions", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ExecutionsService(payload).updateListForeach(payload.body);
  }

  @API("/executions-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ExecutionsService(payload).updateList(payload.body);
  }

  @API("/executions/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ExecutionsService(payload).delete(payload.params);
  }

}
