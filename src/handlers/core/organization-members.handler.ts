import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { OrganizationMembersService } from "../../services/core/organization-members.service";
import { multiple, single } from "../../serializers/core/organization-members.serializer";
import { noSerializer } from "../../utils/utils.service";

export class OrganizationMembersHandler extends CrudHandler {
  
  @API("/organization-members/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new OrganizationMembersService(payload).get(payload.params);;
  }

  @API("/organization-members", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new OrganizationMembersService(payload).list(payload.params);
  }

  @API("/organization-members", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new OrganizationMembersService(payload).create(payload.body);
  }

  @API("/organization-members-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new OrganizationMembersService(payload).createList(payload.body);
  }

  @API("/organization-members/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new OrganizationMembersService(payload).update(payload.body);
  }

  @API("/organization-members", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new OrganizationMembersService(payload).updateListForeach(payload.body);
  }

  @API("/organization-members-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new OrganizationMembersService(payload).updateList(payload.body);
  }

  @API("/organization-members/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new OrganizationMembersService(payload).delete(payload.params);
  }

  @API("/organization-members-list-with-user", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createListUsersAndOrgMembers(payload: Payload) {
    return new OrganizationMembersService(payload).createListUsersAndOrgMembers(payload.body);
  }

}
