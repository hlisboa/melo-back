import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectStatusService } from "../../services/core/project-status.service";
import { multiple, single } from "../../serializers/core/project-status.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectStatusHandler extends CrudHandler {
  
  @API("/project-status/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectStatusService(payload).get(payload.params);;
  }

  @API("/project-status", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectStatusService(payload).list(payload.params);
  }

  @API("/project-status", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectStatusService(payload).create(payload.body);
  }

  @API("/project-status-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectStatusService(payload).createList(payload.body);
  }

  @API("/project-status/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectStatusService(payload).update(payload.body);
  }

  @API("/project-status", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectStatusService(payload).updateListForeach(payload.body);
  }

  @API("/project-status-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectStatusService(payload).updateList(payload.body);
  }

  @API("/project-status/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectStatusService(payload).delete(payload.params);
  }

}
