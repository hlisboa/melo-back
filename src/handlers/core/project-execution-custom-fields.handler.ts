import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectExecutionCustomFieldsService } from "../../services/core/project-execution-custom-fields.service";
import { multiple, single } from "../../serializers/core/project-execution-custom-fields.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectExecutionCustomFieldsHandler extends CrudHandler {
  
  @API("/project-execution-custom-fields/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).get(payload.params);;
  }

  @API("/project-execution-custom-fields", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).list(payload.params);
  }

  @API("/project-execution-custom-fields", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).create(payload.body);
  }

  @API("/project-execution-custom-fields-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).createList(payload.body);
  }

  @API("/project-execution-custom-fields/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).update(payload.body);
  }

  @API("/project-execution-custom-fields", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).updateListForeach(payload.body);
  }

  @API("/project-execution-custom-fields-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).updateList(payload.body);
  }

  @API("/project-execution-custom-fields/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectExecutionCustomFieldsService(payload).delete(payload.params);
  }

}
