import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { OrganizationsService } from "../../services/core/organizations.service";
import { multiple, single } from "../../serializers/core/organizations.serializer";
import { noSerializer } from "../../utils/utils.service";

export class OrganizationsHandler extends CrudHandler {
  
  @API("/organizations/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new OrganizationsService(payload).get(payload.params);;
  }

  @API("/organizations", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new OrganizationsService(payload).list(payload.params);
  }

  @API("/organizations", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new OrganizationsService(payload).create(payload.body);
  }

  @API("/organizations-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new OrganizationsService(payload).createList(payload.body);
  }

  @API("/organizations/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new OrganizationsService(payload).update(payload.body);
  }

  @API("/organizations", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new OrganizationsService(payload).updateListForeach(payload.body);
  }

  @API("/organizations-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new OrganizationsService(payload).updateList(payload.body);
  }

  @API("/organizations/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new OrganizationsService(payload).delete(payload.params);
  }

}
