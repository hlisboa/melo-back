import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ExecutionStatusService } from "../../services/core/execution-status.service";
import { multiple, single } from "../../serializers/core/execution-status.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ExecutionStatusHandler extends CrudHandler {
  
  @API("/execution-status/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ExecutionStatusService(payload).get(payload.params);;
  }

  @API("/execution-status", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ExecutionStatusService(payload).list(payload.params);
  }

  @API("/execution-status", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ExecutionStatusService(payload).create(payload.body);
  }

  @API("/execution-status-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ExecutionStatusService(payload).createList(payload.body);
  }

  @API("/execution-status/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ExecutionStatusService(payload).update(payload.body);
  }

  @API("/execution-status", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ExecutionStatusService(payload).updateListForeach(payload.body);
  }

  @API("/execution-status-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ExecutionStatusService(payload).updateList(payload.body);
  }

  @API("/execution-status/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ExecutionStatusService(payload).delete(payload.params);
  }

}
