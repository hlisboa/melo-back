import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectPlatformsService } from "../../services/core/project-platforms.service";
import { multiple, single } from "../../serializers/core/project-platforms.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectPlatformsHandler extends CrudHandler {
  
  @API("/project-platforms/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectPlatformsService(payload).get(payload.params);;
  }

  @API("/project-platforms", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectPlatformsService(payload).list(payload.params);
  }

  @API("/project-platforms", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectPlatformsService(payload).create(payload.body);
  }

  @API("/project-platforms-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectPlatformsService(payload).createList(payload.body);
  }

  @API("/project-platforms/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectPlatformsService(payload).update(payload.body);
  }

  @API("/project-platforms", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectPlatformsService(payload).updateListForeach(payload.body);
  }

  @API("/project-platforms-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectPlatformsService(payload).updateList(payload.body);
  }

  @API("/project-platforms/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectPlatformsService(payload).delete(payload.params);
  }

}
