import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectTagsService } from "../../services/core/project-tags.service";
import { multiple, single } from "../../serializers/core/project-tags.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectTagsHandler extends CrudHandler {
  
  @API("/project-tags/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectTagsService(payload).get(payload.params);;
  }

  @API("/project-tags", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectTagsService(payload).list(payload.params);
  }

  @API("/project-tags", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectTagsService(payload).create(payload.body);
  }

  @API("/project-tags-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectTagsService(payload).createList(payload.body);
  }

  @API("/project-tags/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectTagsService(payload).update(payload.body);
  }

  @API("/project-tags", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectTagsService(payload).updateListForeach(payload.body);
  }

  @API("/project-tags-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectTagsService(payload).updateList(payload.body);
  }

  @API("/project-tags/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectTagsService(payload).delete(payload.params);
  }

}
