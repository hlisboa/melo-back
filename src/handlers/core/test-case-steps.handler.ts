import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseStepsService } from "../../services/core/test-case-steps.service";
import { multiple, single } from "../../serializers/core/test-case-steps.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseStepsHandler extends CrudHandler {
  
  @API("/test-case-steps/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseStepsService(payload).get(payload.params);;
  }

  @API("/test-case-steps", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseStepsService(payload).list(payload.params);
  }

  @API("/test-case-steps", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseStepsService(payload).create(payload.body);
  }

  @API("/test-case-steps-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseStepsService(payload).createList(payload.body);
  }

  @API("/test-case-steps/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseStepsService(payload).update(payload.body);
  }

  @API("/test-case-steps", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseStepsService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-steps-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseStepsService(payload).updateList(payload.body);
  }

  @API("/test-case-steps/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseStepsService(payload).delete(payload.params);
  }

}
