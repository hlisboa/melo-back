import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestsCycleStatusService } from "../../services/core/tests-cycle-status.service";
import { multiple, single } from "../../serializers/core/tests-cycle-status.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestsCycleStatusHandler extends CrudHandler {
  
  @API("/tests-cycle-status/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestsCycleStatusService(payload).get(payload.params);;
  }

  @API("/tests-cycle-status", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestsCycleStatusService(payload).list(payload.params);
  }

  @API("/tests-cycle-status", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestsCycleStatusService(payload).create(payload.body);
  }

  @API("/tests-cycle-status-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestsCycleStatusService(payload).createList(payload.body);
  }

  @API("/tests-cycle-status/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestsCycleStatusService(payload).update(payload.body);
  }

  @API("/tests-cycle-status", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestsCycleStatusService(payload).updateListForeach(payload.body);
  }

  @API("/tests-cycle-status-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestsCycleStatusService(payload).updateList(payload.body);
  }

  @API("/tests-cycle-status/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestsCycleStatusService(payload).delete(payload.params);
  }

}
