import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectPicturesService } from "../../services/core/project-pictures.service";
import { multiple, single } from "../../serializers/core/project-pictures.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectPicturesHandler extends CrudHandler {
  
  @API("/project-pictures/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectPicturesService(payload).get(payload.params);;
  }

  @API("/project-pictures", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectPicturesService(payload).list(payload.params);
  }

  @API("/project-pictures", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectPicturesService(payload).create(payload.body);
  }

  @API("/project-pictures-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectPicturesService(payload).createList(payload.body);
  }

  @API("/project-pictures/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectPicturesService(payload).update(payload.body);
  }

  @API("/project-pictures", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectPicturesService(payload).updateListForeach(payload.body);
  }

  @API("/project-pictures-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectPicturesService(payload).updateList(payload.body);
  }

  @API("/project-pictures/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectPicturesService(payload).delete(payload.params);
  }

}
