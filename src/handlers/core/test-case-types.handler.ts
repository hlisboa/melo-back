import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseTypesService } from "../../services/core/test-case-types.service";
import { multiple, single } from "../../serializers/core/test-case-types.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseTypesHandler extends CrudHandler {
  
  @API("/test-case-types/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseTypesService(payload).get(payload.params);;
  }

  @API("/test-case-types", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseTypesService(payload).list(payload.params);
  }

  @API("/test-case-types", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseTypesService(payload).create(payload.body);
  }

  @API("/test-case-types-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseTypesService(payload).createList(payload.body);
  }

  @API("/test-case-types/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseTypesService(payload).update(payload.body);
  }

  @API("/test-case-types", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseTypesService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-types-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseTypesService(payload).updateList(payload.body);
  }

  @API("/test-case-types/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseTypesService(payload).delete(payload.params);
  }

}
