import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestsCyclesService } from "../../services/core/tests-cycles.service";
import { multiple, single } from "../../serializers/core/tests-cycles.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestsCyclesHandler extends CrudHandler {
  
  @API("/tests-cycles/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestsCyclesService(payload).get(payload.params);;
  }

  @API("/tests-cycles", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestsCyclesService(payload).list(payload.params);
  }

  @API("/tests-cycles", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestsCyclesService(payload).create(payload.body);
  }

  @API("/tests-cycles-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestsCyclesService(payload).createList(payload.body);
  }

  @API("/tests-cycles/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestsCyclesService(payload).update(payload.body);
  }

  @API("/tests-cycles", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestsCyclesService(payload).updateListForeach(payload.body);
  }

  @API("/tests-cycles-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestsCyclesService(payload).updateList(payload.body);
  }

  @API("/tests-cycles/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestsCyclesService(payload).delete(payload.params);
  }

}
