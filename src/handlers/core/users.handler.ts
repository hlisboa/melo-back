import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { UsersService } from "../../services/core/users.service";
import { multiple, single } from "../../serializers/core/users.serializer";
import { noSerializer } from "../../utils/utils.service";

export class UsersHandler extends CrudHandler {
  
  @API("/users/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new UsersService(payload).get(payload.params);;
  }

  @API("/users", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new UsersService(payload).list(payload.params);
  }

  @API("/users", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new UsersService(payload).create(payload.body);
  }

  @API("/users-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new UsersService(payload).createList(payload.body);
  }

  @API("/users/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new UsersService(payload).update(payload.body);
  }

  @API("/users", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new UsersService(payload).updateListForeach(payload.body);
  }

  @API("/users-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new UsersService(payload).updateList(payload.body);
  }

  @API("/users/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new UsersService(payload).delete(payload.params);
  }

}
