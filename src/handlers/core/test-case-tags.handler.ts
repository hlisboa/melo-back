import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseTagsService } from "../../services/core/test-case-tags.service";
import { multiple, single } from "../../serializers/core/test-case-tags.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseTagsHandler extends CrudHandler {
  
  @API("/test-case-tags/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseTagsService(payload).get(payload.params);;
  }

  @API("/test-case-tags", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseTagsService(payload).list(payload.params);
  }

  @API("/test-case-tags", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseTagsService(payload).create(payload.body);
  }

  @API("/test-case-tags-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseTagsService(payload).createList(payload.body);
  }

  @API("/test-case-tags/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseTagsService(payload).update(payload.body);
  }

  @API("/test-case-tags", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseTagsService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-tags-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseTagsService(payload).updateList(payload.body);
  }

  @API("/test-case-tags/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseTagsService(payload).delete(payload.params);
  }

}
