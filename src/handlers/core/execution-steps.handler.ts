import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ExecutionStepsService } from "../../services/core/execution-steps.service";
import { multiple, single } from "../../serializers/core/execution-steps.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ExecutionStepsHandler extends CrudHandler {
  
  @API("/execution-steps/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ExecutionStepsService(payload).get(payload.params);;
  }

  @API("/execution-steps", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ExecutionStepsService(payload).list(payload.params);
  }

  @API("/execution-steps", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ExecutionStepsService(payload).create(payload.body);
  }

  @API("/execution-steps-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ExecutionStepsService(payload).createList(payload.body);
  }

  @API("/execution-steps/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ExecutionStepsService(payload).update(payload.body);
  }

  @API("/execution-steps", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ExecutionStepsService(payload).updateListForeach(payload.body);
  }

  @API("/execution-steps-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ExecutionStepsService(payload).updateList(payload.body);
  }

  @API("/execution-steps/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ExecutionStepsService(payload).delete(payload.params);
  }

}
