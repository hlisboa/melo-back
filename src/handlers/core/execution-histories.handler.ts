import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ExecutionHistoriesService } from "../../services/core/execution-histories.service";
import { multiple, single } from "../../serializers/core/execution-histories.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ExecutionHistoriesHandler extends CrudHandler {
  
  @API("/execution-histories/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ExecutionHistoriesService(payload).get(payload.params);;
  }

  @API("/execution-histories", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ExecutionHistoriesService(payload).list(payload.params);
  }

  @API("/execution-histories", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ExecutionHistoriesService(payload).create(payload.body);
  }

  @API("/execution-histories-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ExecutionHistoriesService(payload).createList(payload.body);
  }

  @API("/execution-histories/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ExecutionHistoriesService(payload).update(payload.body);
  }

  @API("/execution-histories", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ExecutionHistoriesService(payload).updateListForeach(payload.body);
  }

  @API("/execution-histories-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ExecutionHistoriesService(payload).updateList(payload.body);
  }

  @API("/execution-histories/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ExecutionHistoriesService(payload).delete(payload.params);
  }

}
