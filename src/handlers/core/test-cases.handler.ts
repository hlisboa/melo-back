import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCasesService } from "../../services/core/test-cases.service";
import { multiple, single } from "../../serializers/core/test-cases.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCasesHandler extends CrudHandler {
  
  @API("/test-cases/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCasesService(payload).get(payload.params);;
  }

  @API("/test-cases", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCasesService(payload).list(payload.params);
  }

  @API("/test-cases", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCasesService(payload).create(payload.body);
  }

  @API("/test-cases-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCasesService(payload).createList(payload.body);
  }

  @API("/test-cases/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCasesService(payload).update(payload.body);
  }

  @API("/test-cases", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCasesService(payload).updateListForeach(payload.body);
  }

  @API("/test-cases-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCasesService(payload).updateList(payload.body);
  }

  @API("/test-cases/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCasesService(payload).delete(payload.params);
  }

}
