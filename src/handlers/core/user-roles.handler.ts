import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { UserRolesService } from "../../services/core/user-roles.service";
import { multiple, single } from "../../serializers/core/user-roles.serializer";
import { noSerializer } from "../../utils/utils.service";

export class UserRolesHandler extends CrudHandler {
  
  @API("/user-roles/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new UserRolesService(payload).get(payload.params);;
  }

  @API("/user-roles", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new UserRolesService(payload).list(payload.params);
  }

  @API("/user-roles", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new UserRolesService(payload).create(payload.body);
  }

  @API("/user-roles-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new UserRolesService(payload).createList(payload.body);
  }

  @API("/user-roles/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new UserRolesService(payload).update(payload.body);
  }

  @API("/user-roles", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new UserRolesService(payload).updateListForeach(payload.body);
  }

  @API("/user-roles-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new UserRolesService(payload).updateList(payload.body);
  }

  @API("/user-roles/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new UserRolesService(payload).delete(payload.params);
  }

}
