import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseStepAttachmentsService } from "../../services/core/test-case-step-attachments.service";
import { multiple, single } from "../../serializers/core/test-case-step-attachments.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseStepAttachmentsHandler extends CrudHandler {
  
  @API("/test-case-step-attachments/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).get(payload.params);;
  }

  @API("/test-case-step-attachments", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).list(payload.params);
  }

  @API("/test-case-step-attachments", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).create(payload.body);
  }

  @API("/test-case-step-attachments-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).createList(payload.body);
  }

  @API("/test-case-step-attachments/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).update(payload.body);
  }

  @API("/test-case-step-attachments", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-step-attachments-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).updateList(payload.body);
  }

  @API("/test-case-step-attachments/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseStepAttachmentsService(payload).delete(payload.params);
  }

}
