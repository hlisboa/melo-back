import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseCustomFieldsService } from "../../services/core/test-case-custom-fields.service";
import { multiple, single } from "../../serializers/core/test-case-custom-fields.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseCustomFieldsHandler extends CrudHandler {
  
  @API("/test-case-custom-fields/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).get(payload.params);;
  }

  @API("/test-case-custom-fields", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).list(payload.params);
  }

  @API("/test-case-custom-fields", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).create(payload.body);
  }

  @API("/test-case-custom-fields-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).createList(payload.body);
  }

  @API("/test-case-custom-fields/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).update(payload.body);
  }

  @API("/test-case-custom-fields", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-custom-fields-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).updateList(payload.body);
  }

  @API("/test-case-custom-fields/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseCustomFieldsService(payload).delete(payload.params);
  }

}
