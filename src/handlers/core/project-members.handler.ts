import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectMembersService } from "../../services/core/project-members.service";
import { multiple, single } from "../../serializers/core/project-members.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectMembersHandler extends CrudHandler {
  
  @API("/project-members/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectMembersService(payload).get(payload.params);;
  }

  @API("/project-members", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectMembersService(payload).list(payload.params);
  }

  @API("/project-members", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectMembersService(payload).create(payload.body);
  }

  @API("/project-members-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectMembersService(payload).createList(payload.body);
  }

  @API("/project-members/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectMembersService(payload).update(payload.body);
  }

  @API("/project-members", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectMembersService(payload).updateListForeach(payload.body);
  }

  @API("/project-members-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectMembersService(payload).updateList(payload.body);
  }

  @API("/project-members/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectMembersService(payload).delete(payload.params);
  }

}
