import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ExecutionStepAttachmentsService } from "../../services/core/execution-step-attachments.service";
import { multiple, single } from "../../serializers/core/execution-step-attachments.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ExecutionStepAttachmentsHandler extends CrudHandler {
  
  @API("/execution-step-attachments/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).get(payload.params);;
  }

  @API("/execution-step-attachments", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).list(payload.params);
  }

  @API("/execution-step-attachments", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).create(payload.body);
  }

  @API("/execution-step-attachments-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).createList(payload.body);
  }

  @API("/execution-step-attachments/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).update(payload.body);
  }

  @API("/execution-step-attachments", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).updateListForeach(payload.body);
  }

  @API("/execution-step-attachments-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).updateList(payload.body);
  }

  @API("/execution-step-attachments/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ExecutionStepAttachmentsService(payload).delete(payload.params);
  }

}
