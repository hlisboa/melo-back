import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseFoldersService } from "../../services/core/test-case-folders.service";
import { multiple, single } from "../../serializers/core/test-case-folders.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseFoldersHandler extends CrudHandler {
  
  @API("/test-case-folders/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseFoldersService(payload).get(payload.params);;
  }

  @API("/test-case-folders", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseFoldersService(payload).list(payload.params);
  }

  @API("/test-case-folders", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseFoldersService(payload).create(payload.body);
  }

  @API("/test-case-folders-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseFoldersService(payload).createList(payload.body);
  }

  @API("/test-case-folders/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseFoldersService(payload).update(payload.body);
  }

  @API("/test-case-folders", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseFoldersService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-folders-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseFoldersService(payload).updateList(payload.body);
  }

  @API("/test-case-folders/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseFoldersService(payload).delete(payload.params);
  }

}
