import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ProjectsService } from "../../services/core/projects.service";
import { multiple, single } from "../../serializers/core/projects.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ProjectsHandler extends CrudHandler {
  
  @API("/projects/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ProjectsService(payload).get(payload.params);
  }

  @API("/projects", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ProjectsService(payload).list(payload.params);
  }

  @API("/projects", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ProjectsService(payload).create(payload.body);
  }

  @API("/projects-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ProjectsService(payload).createList(payload.body);
  }

  @API("/projects/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ProjectsService(payload).update(payload.body);
  }

  @API("/projects", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new ProjectsService(payload).updateListForeach(payload.body);
  }

  @API("/projects-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ProjectsService(payload).updateList(payload.body);
  }

  @API("/projects/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ProjectsService(payload).delete(payload.params);
  }

}
