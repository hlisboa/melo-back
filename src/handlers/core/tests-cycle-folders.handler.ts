import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestsCycleFoldersService } from "../../services/core/tests-cycle-folders.service";
import { multiple, single } from "../../serializers/core/tests-cycle-folders.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestsCycleFoldersHandler extends CrudHandler {

  @API(`/tests-cycle-folders/:id`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.READ_ONLY)
  public async get(payload: Payload) {
    return new TestsCycleFoldersService(payload).get(payload.params);;
  }

  @API(`/tests-cycle-folders`, APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.READ_ONLY)
  public async list(payload: Payload) {
    return new TestsCycleFoldersService(payload).list(payload.params);
  }

  @API(`/tests-cycle-folders`, APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.TESTER)
  public async create(payload: Payload) {
    return new TestsCycleFoldersService(payload).create(payload.body);
  }

  @API(`/tests-cycle-folders-list`, APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.TESTER)
  public async createList(payload: Payload) {
    return new TestsCycleFoldersService(payload).createList(payload.body);
  }

  @API(`/tests-cycle-folders/:id`, APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.TESTER)
  public async update(payload: Payload) {
    return new TestsCycleFoldersService(payload).update(payload.body);
  }

  @API(`/tests-cycle-folders-list`, APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.TESTER)
  public async updateList(payload: Payload) {
    return new TestsCycleFoldersService(payload).updateList(payload.body);
  }

  @API(`/tests-cycle-folders/:id`, APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestsCycleFoldersService(payload).delete(payload.params);
  }

}