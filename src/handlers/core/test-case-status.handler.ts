import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { TestCaseStatusService } from "../../services/core/test-case-status.service";
import { multiple, single } from "../../serializers/core/test-case-status.serializer";
import { noSerializer } from "../../utils/utils.service";

export class TestCaseStatusHandler extends CrudHandler {
  
  @API("/test-case-status/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new TestCaseStatusService(payload).get(payload.params);;
  }

  @API("/test-case-status", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new TestCaseStatusService(payload).list(payload.params);
  }

  @API("/test-case-status", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new TestCaseStatusService(payload).create(payload.body);
  }

  @API("/test-case-status-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new TestCaseStatusService(payload).createList(payload.body);
  }

  @API("/test-case-status/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new TestCaseStatusService(payload).update(payload.body);
  }

  @API("/test-case-status", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateListForeach(payload: Payload) {
    return new TestCaseStatusService(payload).updateListForeach(payload.body);
  }

  @API("/test-case-status-list", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new TestCaseStatusService(payload).updateList(payload.body);
  }

  @API("/test-case-status/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new TestCaseStatusService(payload).delete(payload.params);
  }

}
