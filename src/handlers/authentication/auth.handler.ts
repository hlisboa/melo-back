import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { API, Payload } from "../../libs/handler.decorators";
import { AuthService } from "../../services/authentication/auth.service";

export class AuthHandler {

  @API("/login", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async login(payload: Payload) {
    return new AuthService().login(payload);
  }

  @API("/register", APIEnum.POST, Transactional.READ_ONLY, Auth.NO_AUTH, Permission.TESTER)
  public async register(payload: Payload) {
    return new AuthService().register(payload);
  }
}
