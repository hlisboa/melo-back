import { ExpressInstance } from "./libs/express";
import { TestsCycleFoldersHandler } from "./handlers/core/tests-cycle-folders.handler";
import { TestsCyclesHandler } from "./handlers/core/tests-cycles.handler";
import { ExecutionCustomFieldsHandler } from "./handlers/core/execution-custom-fields.handler";
import { ExecutionStatusHandler } from "./handlers/core/execution-status.handler";
import { ExecutionStepAttachmentsHandler } from "./handlers/core/execution-step-attachments.handler";
import { ExecutionStepsHandler } from "./handlers/core/execution-steps.handler";
import { ExecutionsHandler } from "./handlers/core/executions.handler";
import { OrganizationMembersHandler } from "./handlers/core/organization-members.handler";
import { OrganizationsHandler } from "./handlers/core/organizations.handler";
import { ProjectExecutionCustomFieldsHandler } from "./handlers/core/project-execution-custom-fields.handler";
import { ProjectMembersHandler } from "./handlers/core/project-members.handler";
import { ProjectPicturesHandler } from './handlers/core/project-pictures.handler';
import { ProjectPlatformsHandler } from "./handlers/core/project-platforms.handler";
import { ProjectStatusHandler } from "./handlers/core/project-status.handler";
import { ProjectTagsHandler } from "./handlers/core/project-tags.handler";
import { ProjectTestCaseCustomFieldsHandler } from "./handlers/core/project-test-case-custom-fields.handler";
import { ProjectsHandler } from "./handlers/core/projects.handler";
import { TestCaseCustomFieldsHandler } from "./handlers/core/test-case-custom-fields.handler";
import { TestCaseFoldersHandler } from "./handlers/core/test-case-folders.handler";
import { TestCaseStatusHandler } from "./handlers/core/test-case-status.handler";
import { TestCaseStepAttachmentsHandler } from "./handlers/core/test-case-step-attachments.handler";
import { TestCaseStepsHandler } from "./handlers/core/test-case-steps.handler";
import { TestCaseTagsHandler } from "./handlers/core/test-case-tags.handler";
import { TestCaseTypesHandler } from "./handlers/core/test-case-types.handler";
import { TestCasesHandler } from "./handlers/core/test-cases.handler";
import { TestsCycleStatusHandler } from "./handlers/core/tests-cycle-status.handler";
import { UserRolesHandler } from "./handlers/core/user-roles.handler";
import { UsersHandler } from "./handlers/core/users.handler";
import { ExecutionHistoriesHandler } from "./handlers/core/execution-histories.handler";
import { AuthHandler } from "./handlers/authentication/auth.handler";

new AuthHandler();
new TestCaseCustomFieldsHandler();
new TestCaseFoldersHandler();
new TestCaseStatusHandler();
new TestCaseStepAttachmentsHandler();
new TestCaseStepsHandler();
new TestCaseTagsHandler();
new TestCaseTypesHandler();
new TestCasesHandler();

new ExecutionHistoriesHandler();
new ExecutionStepAttachmentsHandler();
new ExecutionStepsHandler();
new ExecutionCustomFieldsHandler();
new ExecutionStatusHandler();
new ExecutionsHandler();

new TestsCyclesHandler();
new TestsCycleFoldersHandler();
new TestsCycleStatusHandler();

new ProjectPicturesHandler();
new ProjectExecutionCustomFieldsHandler();
new ProjectMembersHandler();
new ProjectPlatformsHandler();
new ProjectStatusHandler();
new ProjectTagsHandler();
new ProjectTestCaseCustomFieldsHandler();
new ProjectsHandler();

new OrganizationMembersHandler();
new OrganizationsHandler();

new UserRolesHandler();
new UsersHandler();

ExpressInstance.EXPRESS.listen(3344, () => {
  console.log("Server running on port 3344");
});


