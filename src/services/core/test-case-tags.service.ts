import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.TestCaseTagInclude = {};

export class TestCaseTagsService extends CrudService<Prisma.TestCaseTagCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testCaseTag, include);
  } 
}
