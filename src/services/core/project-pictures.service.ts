import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ProjectPictureInclude = {};

export class ProjectPicturesService extends CrudService<Prisma.ProjectPictureCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().projectPicture, include);
  } 
}
