import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ProjectTagInclude = {};

export class ProjectTagsService extends CrudService<Prisma.ProjectTagCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().projectTag, include);
  } 
}
