import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ProjectMemberInclude = { user: true };

export class ProjectMembersService extends CrudService<Prisma.ProjectMemberCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().projectMember, include);
  }
}
