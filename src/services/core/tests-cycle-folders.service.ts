import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.TestsCycleFolderInclude = { };

export class TestsCycleFoldersService extends CrudService<Prisma.TestsCycleFolderCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testsCycleFolder, include);
  }

  protected async beforeCreate(entity: Prisma.TestsCycleFolderCreateManyInput): Promise<Prisma.TestsCycleFolderCreateManyInput> {
    const query: iFilter = {
      filter: {
        project_id: entity.project_id,
        ...entity.parent_id && { parent_id: entity.parent_id }
      }
    }
    const lastFolder = await this.getOne(query);
    entity.order = lastFolder && lastFolder.order ? lastFolder.order + 1 : 1;

    return entity;
  }
}
