import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.TestsCycleInclude = {};

export class TestsCyclesService extends CrudService<Prisma.TestsCycleCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testsCycle, include);
  }

  protected async afterList(entities: Prisma.TestsCycleCreateManyInput[]): Promise<Prisma.TestsCycleCreateManyInput[]> {
    entities.forEach((entity) => {
      this.getProgress(entity);
    });
    return entities;
  }

  protected async afterGet(entity: Prisma.TestsCycleCreateManyInput): Promise<Prisma.TestsCycleCreateManyInput> {
    (entity as any).progress = this.calculateProgress(entity);
    return entity;
  }

  protected async beforeCreate(entity: Prisma.TestsCycleCreateManyInput): Promise<Prisma.TestsCycleCreateManyInput> {
    const query: iFilter = {
      filter: { folderId: entity.folderId },
      orderBy: { order: "desc" },
    };

    const lastTestsCycle = await this.getOne(query);
    entity.order = lastTestsCycle && lastTestsCycle.order ? lastTestsCycle.order + 1 : 1;

    //FAZER O USUARIO LOGADO!
    entity.createdBy = 1;

    return entity;
  }

  protected async beforeUpdate(entity: Prisma.TestsCycleCreateManyInput): Promise<Prisma.TestsCycleCreateManyInput> {
    // const payload = req.body;
    //     const id = parseInt(req.params.id);
    //     let data;

    //     let update = [];
    //     if (id) {
    //         //um cara só;
    //         update = [{ id, ...payload }];
    //     } else {
    //         //varios caras
    //         update = payload;
    //     }

    //     //verificar quais tem que atualizar o folderId;
    //     const update_without_folderId = update.filter(ref => !ref.folderId);

    //     // se tiver o folderId
    //     const update_with_folderId = update.filter(ref => ref.folderId);

    //     if (update_with_folderId.length) {
    //         const filter = { id: { "in": update_with_folderId.map(x => x.id) } };
    //         const tests_cycles_data = await crud_handler.get_all(prisma.testsCycle, { filter });

    //         tests_cycles_data.contents.forEach(element => {
    //             const index = update_with_folderId.findIndex(ref => ref.id == element.id);
    //             //se tiver a mesma pasta não precisa alterar nada da ordem;

    //             if (index && index > -1 && element.folderId == update_with_folderId[index].folderId) {
    //                 update_without_folderId.push(update_with_folderId[index]);
    //                 update_with_folderId.splice(index, 1);
    //             }
    //         });
    //     }

    //     const update_list = [];
    //     if (update_with_folderId.length) {
    //         const folderIds = [];
    //         update_with_folderId.forEach(element => {
    //             if (!folderIds.includes(element.folderId)) {
    //                 folderIds.push(element.folderId);
    //             }
    //         });

    //         //fazer o update por pasta;
    //         for (let i = 0; i < folderIds.length; i++) {
    //             const folderId = folderIds[i];
    //             const filter = { folderId: folderId };
    //             const last_tests_cycle = await getLastTestsCycleOrder(filter);
    //             const next_order = last_tests_cycle && last_tests_cycle.order ? last_tests_cycle.order + 1 : 1;
    //             const tests_cycles_folder_update = update_with_folderId.filter(ref => ref.folderId == folderId);
    //             tests_cycles_folder_update.sort((a, b) => a.order - b.order);
    //             tests_cycles_folder_update.forEach((element, index) => {
    //                 element.order = next_order + index;
    //                 update_list.push(element);
    //             });
    //         }
    //     }

    //     const count = await crud_handler.update_many(prisma.testsCycle, update_list.concat(update_without_folderId), {});

    //     if (id) {
    //         data = await crud_handler.get_by_id(id);
    //         res.send(data);
    //     } else {
    //         res.send({ count });
    //     }
    return entity;
  }

  getProgress(entity: any) {
    if (!entity.executions) {
      entity.progress = 0;
      return;
    }
    const countDoneExecutions = entity.executions.filter((exec: any) => exec.statusId > 10).length;
    entity.progress = Number(((countDoneExecutions / entity.executions.length) * 100).toFixed(2) || 0);
  }
}
