import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";
import { ExecutionsService } from "./executions.service";

const include: Prisma.TestCaseInclude = {};

export class TestCasesService extends CrudService<Prisma.TestCaseCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testCase, include);
  }

  protected async afterGet(entity: Prisma.TestCaseCreateManyInput): Promise<Prisma.TestCaseCreateManyInput> {

    const executionService = new ExecutionsService(this.payload);

    const query = {
      filter: { test_case_id: entity.id },
      orderBy: { order: "desc" },
      take: 5
    }
    const lastFiveExecutions = executionService.list(query);
    (entity as any). lastFiveExecutions = lastFiveExecutions;
    
    return entity;
  }

  protected async beforeCreate(entity: Prisma.TestCaseCreateManyInput): Promise<Prisma.TestCaseCreateManyInput> {
    const query: iFilter = {
      filter: { folder_id: entity.folder_id },
      orderBy: { order: "desc" }
    }

    const lastTestCase = await this.getOne(query);
    entity.order = lastTestCase && lastTestCase.order ? lastTestCase.order + 1 : 1;

    return entity;
  }
}
