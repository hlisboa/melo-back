import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ProjectStatusInclude = {};

export class ProjectStatusService extends CrudService<Prisma.ProjectStatusCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().projectStatus, include);
  } 
}
