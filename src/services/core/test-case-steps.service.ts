import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.TestCaseStepInclude = {};

export class TestCaseStepsService extends CrudService<Prisma.TestCaseStepCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testCaseStep, include);
  } 
}
