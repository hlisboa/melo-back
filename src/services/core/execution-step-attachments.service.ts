import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ExecutionStepAttachmentInclude = {};

export class ExecutionStepAttachmentsService extends CrudService<Prisma.ExecutionStepAttachmentCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().executionStepAttachment, include);
  } 
}
