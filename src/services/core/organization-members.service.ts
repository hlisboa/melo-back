import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.OrganizationMemberInclude = { user: true };

export class OrganizationMembersService extends CrudService<Prisma.OrganizationMemberCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().organizationMember, include);
  }

  public async createListUsersAndOrgMembers(
    entities: { email: string; userRoleId: number; name: string; organizationId: number }[]
  ) {
    const arrayEmails: string[] = [];
    entities.forEach((element) => {
      if (!arrayEmails.includes(element.email)) arrayEmails.push(element.email);
    });

    let users = await Database.get().user.findMany({ where: { email: { in: arrayEmails } } });
    const newUsersRequest: any[] = [];
    entities.forEach((element) => {
      const index = users.findIndex((ref) => ref.email == element.email);
      if (index == -1) {
        newUsersRequest.push(
          Database.get().user.create({
            data: { email: element.email, name: element.name },
          })
        );
      }
    });
    let newUsers = [];
    if (newUsersRequest.length) newUsers = await Database.get().$transaction(newUsersRequest);
    //FAZER METODO DE MANDAR EMAIL PARA OS USUARIOS; (COGNITO)
    users = users.concat(newUsers);
    const functions: any = [];
    entities.forEach((element) => {
      const index = users.findIndex((ref) => ref.email == element.email);
      const orgMember: Prisma.OrganizationMemberCreateManyInput = {
        organizationId: element.organizationId,
        userId: users[index].id,
        userRoleId: element.userRoleId,
      };
      functions.push(
        Database.get().organizationMember.create({
          data: orgMember,
        })
      );
    });
    let entitySaved = await Database.get().$transaction(functions);
    return { count: entitySaved.length };
  }
}
