import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.UserInclude = {  };

export class UsersService extends CrudService<Prisma.UserCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().user, include);
  }

  // protected async afterList(entities: Prisma.UserCreateManyInput[]): Promise<Prisma.UserCreateManyInput[]> {
  //   await this.aaaa();
  //   return entities;
  // }
}
