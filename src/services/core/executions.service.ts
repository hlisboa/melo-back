import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma } from "@prisma/client";
import { TestCaseStepsService } from "./test-case-steps.service";
import { ExecutionStepsService } from "./execution-steps.service";

const include: Prisma.ExecutionInclude = {};

export class ExecutionsService extends CrudService<Prisma.ExecutionCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().execution, include);
  }

  protected async afterCreate(_: Prisma.ExecutionCreateManyInput, entitySaved: Prisma.ExecutionCreateManyInput): Promise<Prisma.ExecutionCreateManyInput> {

    const testCaseStepService = new TestCaseStepsService(this.payload);
    const executionStepService = new ExecutionStepsService(this.payload);
    const test_case_id = entitySaved.test_case_id;

    const query: iFilter = {
      filter: { test_case_id }
    }
    const testCaseSteps = (await testCaseStepService.list(query)).contents;

    const executionSteps: Prisma.ExecutionStepCreateManyInput[] = [];

    executionSteps.push(
      testCaseSteps.forEach((step: any) => {
        return {
          test_case_step_id: step.id,
          execution_id: entitySaved.id!
        }
      })
    )
    await executionStepService.createList(testCaseSteps);

    return this.entity
  }

  protected async afterCreateList(_: Prisma.ExecutionCreateManyInput[], entitySaved: Prisma.ExecutionCreateManyInput[]): Promise<Prisma.ExecutionCreateManyInput[]> {
    entitySaved.forEach(entity => {
      this.afterCreate(entity, entity);
    })
    return entitySaved;
  }
}
