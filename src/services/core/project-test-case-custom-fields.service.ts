import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.ProjectTestCaseCustomFieldInclude = {};

export class ProjectTestCaseCustomFieldsService extends CrudService<Prisma.ProjectTestCaseCustomFieldCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().projectTestCaseCustomField, include);
  } 
}
