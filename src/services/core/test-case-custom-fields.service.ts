import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.TestCaseCustomFieldInclude = {};

export class TestCaseCustomFieldsService extends CrudService<Prisma.TestCaseCustomFieldCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().testCaseCustomField, include);
  } 
}
