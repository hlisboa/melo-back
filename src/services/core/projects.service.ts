import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";
import { TestCaseFoldersService } from "./test-case-folders.service";
import { ProjectMembersService } from "./project-members.service";
import { TestsCycleFoldersService } from "./tests-cycle-folders.service";

const include: Prisma.ProjectInclude = {  };

export class ProjectsService extends CrudService<Prisma.ProjectCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().project, include);
  }

  protected async beforeCreate(entity: Prisma.ProjectCreateManyInput): Promise<Prisma.ProjectCreateManyInput> {
    entity.createdBy = 1;    
    entity.updatedBy = 1;    
    return entity;
  }

  protected async afterCreate(
    _: Prisma.ProjectCreateManyInput,
    entitySaved: Prisma.ProjectCreateManyInput
  ): Promise<Prisma.ProjectCreateManyInput> {

    const testCaseFoldersService = new TestCaseFoldersService(this.payload);
    const testsCycleFoldersService = new TestsCycleFoldersService(this.payload);
    const projectMembersService = new ProjectMembersService(this.payload)

    const folder: Prisma.TestCaseFolderCreateManyInput | Prisma.TestsCycleFolderCreateManyInput = {
      name: entitySaved.name,
      projectId: entitySaved.id!,
      order: 0,
      isTrash: false,
    };

    const trashFolder: Prisma.TestCaseFolderCreateManyInput | Prisma.TestsCycleFolderCreateManyInput = {
      name: entitySaved.name,
      projectId: entitySaved.id!,
      order: 0,
      isTrash: true,
    };

    const projectMember: Prisma.ProjectMemberCreateManyInput = {
      userId: entitySaved.createdBy,
      projectId: entitySaved.id!,
    };

    await testCaseFoldersService.createList({data: [folder, trashFolder]});
    await testsCycleFoldersService.createList({data: [folder, trashFolder]});
    await projectMembersService.create({data: projectMember});
    return entitySaved;
  }
}
