import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.UserRoleInclude = {};

export class UserRolesService extends CrudService<Prisma.UserRoleCreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().userRole, include);
  } 
}
