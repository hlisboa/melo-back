import { Payload } from "../../libs/handler.decorators";
import { Database } from "../../libs/database";
export class AuthService {
  constructor() { }

  async login(payload: Payload) {
    const data = await Database.get().organizationMember.findMany({
      where: { userId: payload.user!.id! },
      include: {
        organization: true,
        userRole: true,
      },
    });
    return {
      user: payload.user,
      organizationMembers: data,
    };
  }

  async register(payload: Payload) {
    return await Database.get().user.upsert({
      where: { email: payload.body.email },
      update: payload.body,
      create: payload.body
    });
  }
}
