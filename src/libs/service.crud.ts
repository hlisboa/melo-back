// @ts-ignore: Unreachable code error
// @ts-ignore: Unreachable code error
import { v4 } from "uuid";

import { Payload } from "./handler.decorators";
import { NotFoundException } from "../exceptions/not-found.exception";
import { Database } from "./database";
import { createListResponse } from "../utils/utils.service";

export abstract class CrudService<T> {
  constructor(protected payload: Payload, protected entity: any, protected include: any) {}

  public async get(query: iFilter): Promise<T> {
    let data = await this.entity.findFirstOrThrow(query.prisma);
    if (!data) {
      throw new NotFoundException(`Não foi encontrado!`);
    }
    data = await this.afterGet(data);
    return data;
  }

  public async list(query: iFilter) {
    const { where, orderBy } = query.prisma;
    const createFunctions = [];
    createFunctions.push(
      this.entity.count({
        where,
        orderBy,
      })
    );
    createFunctions.push(this.entity.findMany(query.prisma));
    const data = await Database.get().$transaction(createFunctions);
    const response = createListResponse(data);
    response.contents = await this.afterList(response.contents);
    return response;
  }

  public async create(prismaEntity: {data: T}) {
    let entitySaved = null;
    prismaEntity.data = await this.beforeCreate(prismaEntity.data);
    // (entity as any).id = v4();
    // (entity as any).createdBy = 2;
    // (entity as any).updatedBy = 2;
    entitySaved = await this.entity.create(prismaEntity);
    entitySaved = await this.afterCreate(prismaEntity.data, entitySaved);
    return entitySaved;
  }

  public async createList(prismaEntity: {data: T[]}) {
    let entitiesSaved = null;
    prismaEntity.data = await this.beforeCreateList(prismaEntity.data);
    // entity.forEach((element) => {
    //   (element as any).id = v4();
    //   (element as any).createdBy = this.payload.session?.email || "System";
    //   (element as any).updatedBy = this.payload.session?.email || "System";
    // });
    const updateFunctions: any = [];
    const count = await this.entity.createMany(prismaEntity);
    entitiesSaved = await this.afterCreateList(prismaEntity.data);
    return count;
  }

  public async update(prismaEntity: {where: any, data: T}) {
    //BUSCAR A ENTIDADE ANTIGA ANTES DE ALTERAR PARA FAZER ALGO EM AFTERUPDATE TIPO HISTORICO COISA ASSIM....
    let count = 0;
    prismaEntity.data = await this.beforeUpdate(prismaEntity.data);
    let entity = await this.entity.update(prismaEntity);
    entity = await this.afterUpdate(entity, entity);
    return entity;
  }

  public async updateListForeach(prismaEntity: {where: any, data: T}[]) {
    //BUSCAR A ENTIDADE ANTIGA ANTES DE ALTERAR PARA FAZER ALGO EM AFTERUPDATE TIPO HISTORICO COISA ASSIM....
    let entities: T[] = [];
    prismaEntity.forEach((element: any) => {
      entities.push({id: element.where.id, ...element.data})
    });
    entities = await this.beforeUpdateListForeach(entities);
    entities.forEach(element => {
      let index = prismaEntity.findIndex((ref: any) => ref.where.id == (element as any).id);
      if(index > -1) {
        prismaEntity[index].data = element;
      }
    });
    
    //AJUSTA ERRO QUANDO NAO TIVER ID E QUANDO NAO FOR UM ARRAY
    const updateFunctions: any = [];
    prismaEntity.forEach((element: any) => {
      updateFunctions.push(
        this.entity.update(element)
      );
    });
    const count = (await Database.get().$transaction(updateFunctions)).length;
    // entity = await this.afterUpdateList(entity, entity);
    return { count };
  }

  public async updateList(prismaEntity: {where: any[], data: T}) {
    let entitiesSaved: T[] = [];
    //BUSCAR A ENTIDADE ANTIGA ANTES DE ALTERAR PARA FAZER ALGO EM AFTERUPDATE TIPO HISTORICO COISA ASSIM....
    // query.prisma.data = await this.beforeUpdateList(query.prisma.data);
    //AJUSTA ERRO QUANDO NAO TIVER ID E QUANDO NAO FOR UM ARRAY
    this.entity.updateMany(prismaEntity);
    const count = await this.entity.updateMany(prismaEntity);
    // entitiesSaved = await this.afterUpdateList(query.prisma.data, entitiesSaved);
    return count;
  }

  public async delete(query: iFilter) {
    let entity = await this.get(query.prisma.where.id);
    await this.beforeDelete(query.prisma.where.id);
    await this.entity.delete(query.prisma);
    await this.afterDelete(query.prisma.where.id);
    return query.prisma.where.id;
  }

  public async getOne(query: iFilter) {
    const { filter, orderBy } = query;

    return this.entity.findFirst({
      where: filter,
      orderBy,
    });
  }

  protected async afterGet(entity: T): Promise<T> {
    return entity;
  }

  protected async afterList(entities: T[]): Promise<T[]> {
    return entities;
  }

  protected async beforeCreate(entity: T): Promise<T> {
    return entity;
  }

  protected async afterCreate(_: T, entitySaved: T): Promise<T> {
    return entitySaved;
  }

  protected async beforeCreateList(entity: T[]): Promise<T[]> {
    return entity;
  }

  protected async afterCreateList(_: T[]): Promise<T[]> {
    return _;
  }
  

  protected async beforeUpdate(entity: T): Promise<T> {
    return entity;
  }

  protected async afterUpdate(_: T, entitySaved: T): Promise<T> {
    return entitySaved;
  }
  
  protected async beforeUpdateListForeach(entity: T[]): Promise<T[]> {
    return entity;
  }

  protected async afterUpdateListForeach(_: T[], entitySaved: T[]): Promise<T[]> {
    return entitySaved;
  }

  protected async beforeUpdateList(entity: T[]): Promise<T[]> {
    return entity;
  }

  protected async afterUpdateList(_: T[], entitySaved: T[]): Promise<T[]> {
    return entitySaved;
  }

  protected async beforeDelete(id: number): Promise<void> {}

  protected async afterDelete(id: number): Promise<void> {}
}

export interface iFilter {
  take?: number;
  skip?: number;
  filter?: any;
  orderBy?: any;
  include?: any;
  prisma?: any;
}
