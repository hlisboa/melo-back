import { PrismaClient } from ".prisma/client";

const env = 'development';
// @ts-ignore: Unreachable code error
const config = require('../../config/config.json')[env];
// @ts-ignore: Unreachable code error

export class Database {

  public static prisma: PrismaClient;
  
  public static get() {
    if (!Database.prisma) {
      Database.initialize();
    }
    return Database.prisma;
  }

  public static initialize() {
    try {
      if (!Database.prisma) {
        Database.doInstance();
      }
    } catch (err) {
      console.log('Database connection error!');
      console.log(err);
    }
  }

  public static end() {
    if (Database.prisma) {
      return Database.prisma.$disconnect();
    }
  }

  private static doInstance() {
    Database.prisma = new PrismaClient();
  }
}
