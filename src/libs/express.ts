import { Express } from 'express';
// @ts-ignore: Unreachable code error

export class ExpressInstance {
  
  public static EXPRESS: Express;

  public static get() {
    return ExpressInstance.EXPRESS;
  }

}