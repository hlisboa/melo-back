import { NotFoundException } from './../exceptions/not-found.exception';
import { UnauthorizedException } from "./../exceptions/unauthorized.exception";
import { CognitoJwtVerifier } from "aws-jwt-verify";
import { Database } from "./database";

export async function getVerifiedUser(token: string) {
  const verifier = CognitoJwtVerifier.create({
    userPoolId: "us-east-1_7KcwN7cUn",
    clientId: "1ltsgeasqb6h1p8rngqab1qfk",
    tokenUse: "id",
  });
  let data: any;
  try {
    data = await verifier.verify(token.replace("Bearer ", ""));
  } catch (err) {
    throw new UnauthorizedException("Token not valid!");
  }
  const user = await Database.get().user.findFirst({ where: { email: data["email"] } });
  if(!user) throw new NotFoundException("Usuário não existe.");
  if (!user.confirmed) {
    await Database.get().user.update({
      where: { id: user.id },
      data: { confirmed: true },
    });
  }
  if (user.isDisabled) {
    throw new UnauthorizedException("Usuário desabilitado!");
  }
  return user;
}
