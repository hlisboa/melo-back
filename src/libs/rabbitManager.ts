import { ConflictedException } from "../exceptions/conflicted.exception";
import client, { Channel, Connection } from "amqplib";

export class RabbitManager {

  connection?: Connection;
  channel?: Channel;

  constructor() {
    this.startRabbit();
  }

  async startRabbit() {
    try {
      this.connection = await client.connect(
        process.env.EXPRESS_RABBIT_URL!
      );
      console.log("Rabbit conectado");
      this.channel = await this.connection.createChannel();
      console.log("Rabbit channel");
    } catch (error) {
      console.log(error);
      throw new ConflictedException("Erro ao inicializar o Rabbit");
    }
  }
}
