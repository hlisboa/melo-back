// @ts-ignore: Unreachable code error
import express from "express";
import { Request, Response } from "express";
import { APIEnum } from "../enuns/api.enum";
import { Auth } from "../enuns/auth.enum";
import { Permission } from "../enuns/permission.enum";
import { Transactional } from "../enuns/transactional.enum";
import { Database } from "./database";
import { ExpressInstance } from "./express";
import { CrudHandler } from "./handler.crud";
import { Interceptor } from "./interceptor";
import cors from "cors";
import { parseParams } from "../utils/destructure-query.service";
import { Prisma } from "@prisma/client";
import { getVerifiedUser } from "./cognitoVerifier";
import { ForbiddenException } from "../exceptions/forbidden.exception";

// const bodyParser = require('body-parser');
const options = {
  origin: "*",
  optionsSuccessStatus: 200,
};

export const API =
  // eslint-disable-next-line max-len


  (
    endpoint: string,
    type: APIEnum,
    transactional = Transactional.TRANSACTION,
    auth = Auth.REQUIRED,
    permission = Permission.READ_ONLY
  ) =>
    (className: any, _2: string, propDesc: PropertyDescriptor) => {
      if (!ExpressInstance.EXPRESS) {
        ExpressInstance.EXPRESS = express();
        ExpressInstance.EXPRESS.use(express.json({ limit: "10mb" }));
        ExpressInstance.EXPRESS.use(express.urlencoded({ extended: true }));
        ExpressInstance.EXPRESS.use(cors(options));
      }
      switch (type) {
        case APIEnum.POST:
          ExpressInstance.get().post(endpoint, async (req: Request, res: Response) =>
            res.send(await processRequest(className, propDesc.value, req, res, transactional, auth, permission))
          );
          break;
        case APIEnum.PATCH:
          ExpressInstance.get().patch(endpoint, async (req: Request, res: Response) =>
            res.send(await processRequest(className, propDesc.value, req, res, transactional, auth, permission))
          );
          break;
        case APIEnum.DELETE:
          ExpressInstance.get().delete(endpoint, async (req: Request, res: Response) =>
            res.send(await processRequest(className, propDesc.value, req, res, transactional, auth, permission))
          );
          break;
        case APIEnum.GET:
        default:
          ExpressInstance.get().get(endpoint, async (req: Request, res: Response) =>
            res.send(await processRequest(className, propDesc.value, req, res, transactional, auth, permission))
          );
          break;
      }
    };

const processRequest = async (
  instance: CrudHandler,
  method: Function,
  req: Request,
  res: Response,
  transactional = Transactional.TRANSACTION,
  auth = Auth.REQUIRED,
  permission = Permission.READ_ONLY
) => {
  const payload = new Payload(Number(req.params.id), req.body, parseParams(req.query), req, res);
  try {
    if (auth === Auth.REQUIRED && !req.headers["authorization"]) {
      throw new ForbiddenException('You are not authenticated!');
    }
    if (auth === Auth.REQUIRED && req.headers["authorization"]) {
      //VERIFICAR A SESSÃO E VERIFICAR A PERMISSÂO EM RELAÇÂO AO METODO
      Database.initialize();
      payload.user = await getVerifiedUser(req.headers["authorization"]);
    }
    if (auth === Auth.NO_AUTH) {
      Database.initialize();
    }

    const response = await method(payload);

    // Database.end();
    return response;
  } catch (err) {
    console.log(err);
    Interceptor.map(res, err);
  }
};

export class Payload {
  constructor(
    public id: number,
    public body: any,
    public params: any,
    public request: Request,
    public response: Response,
    public session?: any,
    public user?: Prisma.UserCreateManyInput,
    public transaction?: any
  ) { }
}
