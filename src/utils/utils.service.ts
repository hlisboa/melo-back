export function createListResponse(data: any[]) {
  return { count: data[0], contents: data[1] };
}

export function noSerializer(data: any) {
  return data;
}

export function  serializerResponse(serializer: Function, data: any){
  if (data.contents) {
    data.contents = serializer(data.contents);
    return data;
  } else {
    return serializer(data);
  }
};
