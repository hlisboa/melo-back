function createFilterWhereClauses(filter: any) {
  const where: any = {};
  if (filter.AND && filter.AND.length) {
    where.AND = [];
    filter.AND.forEach((element: any) => {
      if (element.relation) {
        where.AND.push({
          [element.relation]: element.typeRelation
            ? { [element.typeRelation]: createFilterWhereClauses(element.filterRelation) }
            : createFilterWhereClauses(element.filterRelation),
        });
      } else {
        injectWhereClauses(where.AND, element);
      }
    });
  }

  if (filter.OR && filter.OR.length) {
    where.OR = [];
    filter.OR.forEach((element: any) => {
      if (element.relation) {
        where.OR.push({
          [element.relation]: element.typeRelation
            ? { [element.typeRelation]: createFilterWhereClauses(element.filterRelation) }
            : createFilterWhereClauses(element.filterRelation),
        });
      } else {
        injectWhereClauses(where.OR, element);
      }
    });
  }
  return where;
}

function injectWhereClauses(arrayAdd: any[], condition: any) {
  if (condition.op === "eq") {
    if (typeof condition.value === "boolean") {
      arrayAdd.push({
        [condition.key]: condition.value,
      });
    } else {
      arrayAdd.push({
        [condition.key]: parseIfInt(condition.value),
      });
    }
  } else if (condition.op === "isNull") {
    arrayAdd.push({
      [condition.key]: null,
    });
  } else {
    arrayAdd.push({
      [condition.key]: { [condition.op]: parseIfInt(condition.value) },
    });
  }
}

function parseIfInt(value: any) {
  return value.includes("'") ? value.replaceAll("'", "") : parseInt(value);
}

function createOrderByClauses(orderBy: any) {
  const order: any[] = [];
  orderBy.forEach((element: any) => {
    if (element.relation) {
      order.push({ [element.relation]: element.value });
    } else {
      order.push(element.value);
    }
  });
  return order;
}

function createIncludeClauses(data: any) {
  const include: any = {};
  Object.keys(data).forEach((element) => {
    if (typeof data[element] == "boolean") {
      include[element] = true;
    } else if (data[element].include) {
      include[element] = { include: createIncludeClauses(data[element].include) };
    } else {
      include[element] = { where: createFilterWhereClauses(data[element]) };
    }
  });
  return include;
}

export function destructureQuery(query: any) {
  const filter = query["filter"] ? createFilterWhereClauses(JSON.parse(query["filter"])) : {};
  const response: any = {
    filter,
    orderBy: query["orderBy"] ? createOrderByClauses(JSON.parse(query["orderBy"])) : {},
    include: query["include"] ? createIncludeClauses(JSON.parse(query["include"])) : null,
  };
  if (query["skip"]) {
    response.skip = parseInt(query["skip"]);
  }
  if (query["take"]) {
    response.take = parseInt(query["take"]);
  }
  return response;
}

export function parseParams(query: any) {
  return { prisma: query.prisma ? JSON.parse(query.prisma) : {} };
}
