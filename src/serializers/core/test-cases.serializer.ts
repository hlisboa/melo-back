import { single as usersSerializer } from "./users.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.description = data.description;
  response.preRequirements = data.pre_requirements;
  response.expectedResult = data.expected_result;
  response.generalEvidence_file_name = data.general_evidence_file_name;
  response.scenario = data.scenario;
  response.type = data.type;
  response.statusId = data.status_id;
  response.createdByUser = data.createdBy_user ? usersSerializer(data.createdBy_user) : null;
  response.updatedByUser = data.updatedBy_user ? usersSerializer(data.updatedBy_user) : null;
  response.projectId = data.projectId;
  response.createdAt = data.created_at;
  response.updatedAt = data.updated_at;
  response.folderId = data.folderId;
  // response.customFields = data.custom_fields ? project_test_case_custom_field_serializer(data.custom_fields) : null;
  // response.steps = data.test_case_steps ? test_case_step_serializer(data.test_case_steps) : null;
  response.order = data.order;
  response.status = data.status;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
