export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.testCaseId = data.test_case_id;
  response.name = data.name;
  response.projectId = data.projectId;
  response.parentId = data.parent_id;
  response.order = data.order;
  response.isTrash = data.isTrash;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
