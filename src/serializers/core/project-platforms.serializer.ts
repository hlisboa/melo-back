import { single as projectsSerializer } from "./projects.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.description = data.description;
  response.projectId = data.projectId;

  //relation
  response.project = data.project ? projectsSerializer(data.project) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
