export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.companyName = data.company_name;
  response.employeeQuantity = data.employee_quantity;
  response.location = data.location;
  response.companySegment = data.company_segment;
  response.ownerId = data.owner_id;
  response.trialStatus = data.trial_status;
  response.trialsDays = data.trials_days;
  response.createdAt = data.created_at;
  response.updatedAt = data.updated_at;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
