import { multiple as projetcMembersSerializer } from "./project-members.serializer";
import { multiple as organizationMembersSerializer } from "./organization-members.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.email = data.email;
  response.name = data.name;
  response.company = data.company;
  response.provider = data.provider;
  response.statusId = data.status_id;
  response.confirmed = data.confirmed;
  response.userFuction = data.user_fuction;
  response.userDepartment = data.user_department;
  response.userLocation = data.user_location;
  response.isDisabled = data.is_disabled;

  //relations
  response.profilePictureUrl = data.profile_picture_url ? JSON.parse(data.profile_picture_url) : null;
  response.projectMembers = data.project_member ? projetcMembersSerializer(data.project_member) : null;
  response.organizarionMember = data.organization_member ? organizationMembersSerializer(data.organization_member) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
