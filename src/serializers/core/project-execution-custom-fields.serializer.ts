import { single as projectsSerializer } from "./projects.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.formName = data.form_name;
  response.type = data.type;
  response.projectId = data.projectId;
  response.isDisabled = data.is_disabled;
  response.isRequired = data.is_required;
  response.selectOptions = data.select_options;
  response.createdAt = data.created_at;
  response.updatedAt = data.updated_at;

  //relation
  response.project = data.project ? projectsSerializer(data.project) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
