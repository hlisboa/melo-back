export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.url = data.url;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
