export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.type = data.type;
  response.name = data.name;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
