import { single as usersSerializer } from "./users.serializer";
import { single as userRolesSerializer } from "./user-roles.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.userId = data.userId;
  response.organizationId = data.organizationId;
  response.userRoleId = data.userRoleId;
  response.isActive = data.is_active;
  //relation
  response.user = data.user ? usersSerializer(data.user) : null;
  response.userRole = data.user_role ? userRolesSerializer(data.user_role) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
