import { single as usersSerializer } from "./users.serializer";


export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.userId = data.userId;
  response.projectId = data.projectId;
  response.isDeleted = data.is_deleted;
  response.isActive = data.is_active;

  //relation
  response.user = data.user ? usersSerializer(data.user) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
