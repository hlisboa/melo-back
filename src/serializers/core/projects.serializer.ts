import { single as projetcPicturesSerializer } from "./project-pictures.serializer";
import { multiple as projetcMembersSerializer } from "./project-members.serializer";
import { single as usersSerializer } from "./users.serializer";
import { multiple as projectTestCaseCustomFieldsSerializer } from "./project-test-case-custom-fields.serializer";
import { multiple as projectExecutiomCustomFieldsSerializer } from "./project-execution-custom-fields.serializer";
import { multiple as projectPlatformsSerializer } from "./project-platforms.serializer";
import { multiple as executionStatusSerializer } from "./execution-status.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.description = data.description;
  response.statusId = data.status_id;
  response.createdBy = data.updatedBy;
  response.createdAt = data.created_at;
  response.updatedBy = data.updatedBy;
  response.updatedAt = data.updated_at;
  response.dataPictureUrl = data.project_picture_url;
  response.projectBackgroundColor = data.project_background_color;

  //relations
  response.projectPicture = data.project_picture ? projetcPicturesSerializer(data.project_picture) : null;
  response.projectMembers = data.project_members ? projetcMembersSerializer(data.project_members) : [];
  response.createdByUser = data.createdBy_user ? usersSerializer(data.createdBy_user) : null;
  response.updatedByUser = data.updatedBy_user ? usersSerializer(data.updatedBy_user) : null;

  response.testCaseCustomFields = data.test_case_custom_fields ? projectTestCaseCustomFieldsSerializer(data.test_case_custom_fields) : [];
  response.projectPlatforms = data.project_platforms ? projectPlatformsSerializer(data.project_platforms) : [];
  response.executionCustomFields = data.execution_custom_fields ? projectExecutiomCustomFieldsSerializer(data.execution_custom_fields) : [];
  response.executionStatus = data.execution_status ? executionStatusSerializer(data.execution_status) : [];

  //fazer
  // response.organization = data.organization ? usersSerializer(data.organization) : null;
  // response.projectStatus = data.project_status ? usersSerializer(data.project_status) : null;
  // response.testCases = data.test_cases ? usersSerializer(data.test_cases) : [];
  // response.testCaseFolders = data.test_case_folders ? usersSerializer(data.test_case_folders) : null;
  // response.testsCycleFolders = data.tests_cycle_folders ? usersSerializer(data.tests_cycle_folders) : null;
  // response.projectTags = data.project_tags ? usersSerializer(data.project_tags) : null;
  // response.projectTestsCycles = data.project_tests_cycles ? usersSerializer(data.project_tests_cycles) : null;
  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}
