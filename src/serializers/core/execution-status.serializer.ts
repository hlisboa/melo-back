import { single as projectsSerializer } from "./projects.serializer";

export function single(data: any) {
  let response: any = {};
  response.id = data.id;
  response.name = data.name;
  response.description = data.description;
  response.color = data.color;
  response.projectId = data.projectId;
  response.isDefault =  data.is_default;
  response.isDeleted =  data.is_deleted;
  response.deletedBy =  data.deleted_by;
  response.deletedAt =  data.deleted_at;

  //relation
  response.project = data.project ? projectsSerializer(data.project) : null;

  return response;
}

export function multiple(data: any) {
  return data.map((x: any) => single(x));
}

