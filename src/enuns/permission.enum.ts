export enum Permission {
  ORG_ADM = "org_adm",
  PROJECT_ADM = "project_adm",
  TESTER = "tester",
  READ_ONLY = "ready_only",
  MELOQA_ADM = "meloqa_adm",
}
