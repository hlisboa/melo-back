export enum APIEnum {
  GET,
  POST,
  PATCH,
  DELETE,
}
