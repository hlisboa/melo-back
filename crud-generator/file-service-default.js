
module.exports.serivce = (className, classNamePrismaModel, classNamePrismaDatabase) => {
return `import { iFilter } from "../../libs/service.crud";
import { Payload } from "../../libs/handler.decorators";
import { CrudService } from "../../libs/service.crud";
import { Database } from "../../libs/database";
import { Prisma, PrismaClient } from "@prisma/client";

const include: Prisma.${classNamePrismaModel}Include = {};

export class ${className}Service extends CrudService<Prisma.${classNamePrismaModel}CreateManyInput> {
  constructor(payload: Payload) {
    super(payload, Database.get().${classNamePrismaDatabase}, include);
  } 
}
`;
}