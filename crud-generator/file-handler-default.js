
module.exports.handler = (className, pathServiceName) => {
return `import { APIEnum } from "../../enuns/api.enum";
import { Auth } from "../../enuns/auth.enum";
import { Permission } from "../../enuns/permission.enum";
import { Transactional } from "../../enuns/transactional.enum";
import { CrudHandler } from "../../libs/handler.crud";
import { API, Payload } from "../../libs/handler.decorators";
import { ${className}Service } from "../../services/core/${pathServiceName}.service";
import { multiple, single } from "../../serializers/core/${pathServiceName}.serializer";
import { noSerializer } from "../../utils/utils.service";

export class ${className}Handler extends CrudHandler {
  
  @API("/${pathServiceName}/:id", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async get(payload: Payload) {
    return new ${className}Service(payload).get(payload.params);;
  }

  @API("/${pathServiceName}", APIEnum.GET, Transactional.READ_ONLY, Auth.REQUIRED, Permission.TESTER)
  public async list(payload: Payload) {
    return new ${className}Service(payload).list(payload.params);
  }

  @API("/${pathServiceName}", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async create(payload: Payload) {
    return new ${className}Service(payload).create(payload.body);
  }

  @API("/${pathServiceName}-list", APIEnum.POST, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async createList(payload: Payload) {
    return new ${className}Service(payload).createList(payload.body);
  }

  @API("/${pathServiceName}/:id", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async update(payload: Payload) {
    return new ${className}Service(payload).update(payload.body);
  }

  @API("/${pathServiceName}", APIEnum.PATCH, Transactional.TRANSACTION, Auth.REQUIRED, Permission.PROJECT_ADM)
  public async updateList(payload: Payload) {
    return new ${className}Service(payload).updateList(payload.body);
  }

  @API("/${pathServiceName}/:id", APIEnum.DELETE, Transactional.TRANSACTION, Auth.REQUIRED, Permission.ORG_ADM)
  public async delete(payload: Payload) {
    return new ${className}Service(payload).delete(payload.params);
  }

}
`;
}