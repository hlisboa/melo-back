module.exports = {
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "rules": {
        "semi": ["error", "always"],
        "quotes": ["error", "double"]
    },
    "extends": "eslint:recommended",
    "env": {
        "browser": true,
        "node": true,
        "es6": true
      }
};